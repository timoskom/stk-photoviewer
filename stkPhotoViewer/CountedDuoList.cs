﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace stkPhotoViewer
{
    public class CountedDuoList<T_List1, T_List2>
    {
        #region Private Data
        private List<T_List1> ListOne { get; set; }
        private List<T_List2> ListTwo { get; set; }
        #endregion // Private Data

        #region Public Data
        public int Count
        {
            get => ListOne.Count;
        }

        public int Capacity
        {
            get => ListOne.Capacity;
            set => ListOne.Capacity = ListTwo.Capacity = value;
        }
        #endregion // Public Data

        #region Events
        public event EventHandler<CountChangeEventArgs> CountChanged;
        #endregion // Events

        #region Constructors
        public CountedDuoList()
        {
            ListOne = new List<T_List1>();
            ListTwo = new List<T_List2>();
        }

        public CountedDuoList(IEnumerable<T_List1> collectionOne, IEnumerable<T_List2> collectionTwo)
        {
            ListOne = new List<T_List1>(collectionOne);
            ListTwo = new List<T_List2>(collectionTwo);
        }

        public CountedDuoList(int capacity)
        {
            ListOne = new List<T_List1>(capacity);
            ListTwo = new List<T_List2>(capacity);
        }
        #endregion // Constructors

        #region Private Methods
        #endregion // Private Methods

        #region Protected Methods
        #endregion // Protected Methods

        #region Public Methods
        public void Add(T_List1 itemOne, T_List2 itemTwo)
        {
            ListOne.Add(itemOne);
            ListTwo.Add(itemTwo);
            CountChanged?.Invoke(this, new CountChangeEventArgs(this.Count));
        }

        public void AddRange(IEnumerable<T_List1> collectionOne, IEnumerable<T_List2> collectionTwo)
        {
            ListOne.AddRange(collectionOne);
            ListTwo.AddRange(collectionTwo);
            CountChanged?.Invoke(this, new CountChangeEventArgs(this.Count));
        }

        public System.Collections.ObjectModel.ReadOnlyCollection<T_List1> One_AsReadOnly()
        {
            return ListOne.AsReadOnly();
        }

        public System.Collections.ObjectModel.ReadOnlyCollection<T_List2> Two_AsReadOnly()
        {
            return ListTwo.AsReadOnly();
        }

        public int BinarySearch(int index, int count, T_List1 item, IComparer<T_List1> comparer)
        {
            return ListOne.BinarySearch(index, count, item, comparer);
        }

        public int BinarySearch(int index, int count, T_List2 item, IComparer<T_List2> comparer)
        {
            return ListTwo.BinarySearch(index, count, item, comparer);
        }

        public int BinarySearch(T_List1 item)
        {
            return ListOne.BinarySearch(item);
        }

        public int BinarySearch(T_List2 item)
        {
            return ListTwo.BinarySearch(item);
        }

        public int BinarySearch(T_List1 item, IComparer<T_List1> comparer)
        {
            return ListOne.BinarySearch(item, comparer);
        }

        public int BinarySearch(T_List2 item, IComparer<T_List2> comparer)
        {
            return ListTwo.BinarySearch(item, comparer);
        }

        public void Clear()
        {
            ListOne.Clear();
            ListTwo.Clear();
            CountChanged?.Invoke(this, new CountChangeEventArgs(this.Count));
        }

        public bool Contains(T_List1 item)
        {
            return ListOne.Contains(item);
        }

        public bool Contains(T_List2 item)
        {
            return ListTwo.Contains(item);
        }

        public List<TOutput> One_ConvertAll<TOutput>(Converter<T_List1, TOutput> converter)
        {
            return ListOne.ConvertAll(converter);
        }

        public List<TOutput> Two_ConvertAll<TOutput>(Converter<T_List2, TOutput> converter)
        {
            return ListTwo.ConvertAll(converter);
        }

        public void One_CopyTo(int index, T_List1[] array, int arrayIndex, int count)
        {
            ListOne.CopyTo(index, array, arrayIndex, count);
        }

        public void Two_CopyTo(int index, T_List2[] array, int arrayIndex, int count)
        {
            ListTwo.CopyTo(index, array, arrayIndex, count);
        }

        public void One_CopyTo(T_List1[] array)
        {
            ListOne.CopyTo(array);
        }

        public void Two_CopyTo(T_List2[] array)
        {
            ListTwo.CopyTo(array);
        }

        public void One_CopyTo(T_List1[] array, int index)
        {
            ListOne.CopyTo(array, index);
        }

        public void Two_CopyTo(T_List2[] array, int index)
        {
            ListTwo.CopyTo(array, index);
        }

        public bool Exists(Predicate<T_List1> match)
        {
            return ListOne.Exists(match);
        }

        public bool Exists(Predicate<T_List2> match)
        {
            return ListTwo.Exists(match);
        }

        public T_List1 Find(Predicate<T_List1> match)
        {
            return ListOne.Find(match);
        }

        public T_List2 Find(Predicate<T_List2> match)
        {
            return ListTwo.Find(match);
        }

        public List<T_List1> FindAll(Predicate<T_List1> match)
        {
            return ListOne.FindAll(match);
        }

        public List<T_List2> FindAll(Predicate<T_List2> match)
        {
            return ListTwo.FindAll(match);
        }

        public int FindIndex(int startIndex, int count, Predicate<T_List1> match)
        {
            return ListOne.FindIndex(startIndex, count, match);
        }

        public int FindIndex(int startIndex, int count, Predicate<T_List2> match)
        {
            return ListTwo.FindIndex(startIndex, count, match);
        }

        public int FindIndex(int startIndex, Predicate<T_List1> match)
        {
            return ListOne.FindIndex(startIndex, match);
        }

        public int FindIndex(int startIndex, Predicate<T_List2> match)
        {
            return ListTwo.FindIndex(startIndex, match);
        }

        public int FindIndex(Predicate<T_List1> match)
        {
            return ListOne.FindIndex(match);
        }

        public int FindIndex(Predicate<T_List2> match)
        {
            return ListTwo.FindIndex(match);
        }

        public T_List1 FindLast(Predicate<T_List1> match)
        {
            return ListOne.FindLast(match);
        }

        public T_List2 FindLast(Predicate<T_List2> match)
        {
            return ListTwo.FindLast(match);
        }

        public int FindLastIndex(int startIndex, int count, Predicate<T_List1> match)
        {
            return ListOne.FindLastIndex(startIndex, count, match);
        }

        public int FindLastIndex(int startIndex, int count, Predicate<T_List2> match)
        {
            return ListTwo.FindLastIndex(startIndex, count, match);
        }

        public int FindLastIndex(int startIndex, Predicate<T_List1> match)
        {
            return ListOne.FindLastIndex(startIndex, match);
        }

        public int FindLastIndex(int startIndex, Predicate<T_List2> match)
        {
            return ListTwo.FindLastIndex(startIndex, match);
        }

        public int FindLastIndex(Predicate<T_List1> match)
        {
            return ListOne.FindLastIndex(match);
        }

        public int FindLastIndex(Predicate<T_List2> match)
        {
            return ListTwo.FindLastIndex(match);
        }

        public void One_ForEach(Action<T_List1> action)
        {
            ListOne.ForEach(action);
        }

        public void Two_ForEach(Action<T_List2> action)
        {
            ListTwo.ForEach(action);
        }

        public List<T_List1>.Enumerator One_GetEnumerator()
        {
            return ListOne.GetEnumerator();
        }

        public List<T_List2>.Enumerator Two_GetEnumerator()
        {
            return ListTwo.GetEnumerator();
        }

        public List<T_List1> One_GetRange(int index, int count)
        {
            return ListOne.GetRange(index, count);
        }

        public List<T_List2> Two_GetRange(int index, int count)
        {
            return ListTwo.GetRange(index, count);
        }

        public int IndexOf(T_List1 item)
        {
            return ListOne.IndexOf(item);
        }

        public int IndexOf(T_List2 item)
        {
            return ListTwo.IndexOf(item);
        }

        public int IndexOf(T_List1 item, int index)
        {
            return ListOne.IndexOf(item, index);
        }

        public int IndexOf(T_List2 item, int index)
        {
            return ListTwo.IndexOf(item, index);
        }

        public int IndexOf(T_List1 item, int index, int count)
        {
            return ListOne.IndexOf(item, index, count);
        }

        public int IndexOf(T_List2 item, int index, int count)
        {
            return ListTwo.IndexOf(item, index, count);
        }

        public void Insert(int index, T_List1 itemOne, T_List2 itemTwo)
        {
            ListOne.Insert(index, itemOne);
            ListTwo.Insert(index, itemTwo);
            CountChanged?.Invoke(this, new CountChangeEventArgs(this.Count));
        }

        public void InsertRange(int index, IEnumerable<T_List1> collectionOne, IEnumerable<T_List2> collectionTwo)
        {
            ListOne.InsertRange(index, collectionOne);
            ListTwo.InsertRange(index, collectionTwo);
            CountChanged?.Invoke(this, new CountChangeEventArgs(this.Count));
        }

        public int LastIndexOf(T_List1 item)
        {
            return ListOne.LastIndexOf(item);
        }

        public int LastIndexOf(T_List2 item)
        {
            return ListTwo.LastIndexOf(item);
        }

        public int LastIndexOf(T_List1 item, int index)
        {
            return ListOne.LastIndexOf(item, index);
        }

        public int LastIndexOf(T_List2 item, int index)
        {
            return ListTwo.LastIndexOf(item, index);
        }

        public int LastIndexOf(T_List1 item, int index, int count)
        {
            return ListOne.LastIndexOf(item, index, count);
        }

        public int LastIndexOf(T_List2 item, int index, int count)
        {
            return ListTwo.LastIndexOf(item, index, count);
        }

        public bool Remove(T_List1 item)
        {
            int index = ListOne.IndexOf(item);
            ListTwo.RemoveAt(index);
            return ListOne.Remove(item);
        }

        public bool Remove(T_List2 item)
        {
            int index = ListTwo.IndexOf(item);
            ListOne.RemoveAt(index);
            bool result = ListTwo.Remove(item);
            CountChanged?.Invoke(this, new CountChangeEventArgs(this.Count));
            return result;
        }

        //public int RemoveAll(Predicate<T_List1> match);
        //public int RemoveAll(Predicate<T_List2> match);

        public void RemoveAt(int index)
        {
            ListOne.RemoveAt(index);
            ListTwo.RemoveAt(index);
            CountChanged?.Invoke(this, new CountChangeEventArgs(this.Count));
        }

        public void RemoveRange(int index, int count)
        {
            ListOne.RemoveRange(index, count);
            ListTwo.RemoveRange(index, count);
            CountChanged?.Invoke(this, new CountChangeEventArgs(this.Count));
        }

        public void Reverse()
        {
            ListOne.Reverse();
            ListTwo.Reverse();
        }

        public void Reverse(int index, int count)
        {
            ListOne.Reverse(index, count);
            ListTwo.Reverse(index, count);
        }

        //public void Sort();
        //public void Sort (Comparison<T_List1> comparison);
        //public void Sort (Comparison<T_List2> comparison);
        //public void Sort(IComparer<T_List1> comparer);
        //public void Sort(IComparer<T_List2> comparer);
        //public void Sort(int index, int count, IComparer<T_List1> comparer);
        //public void Sort(int index, int count, IComparer<T_List2> comparer);

        public T_List1[] One_ToArray()
        {
            return ListOne.ToArray();
        }

        public T_List2[] Two_ToArray()
        {
            return ListTwo.ToArray();
        }

        public void TrimExcess()
        {
            ListOne.TrimExcess();
            ListTwo.TrimExcess();
        }

        public bool One_TrueForAll(Predicate<T_List1> match)
        {
            return ListOne.TrueForAll(match);
        }

        public bool Two_TrueForAll(Predicate<T_List2> match)
        {
            return ListTwo.TrueForAll(match);
        }

        public T_List1 One_GetAt(int index)
        {
            return ListOne.ElementAt(index);
        }
        public T_List2 Two_GetAt(int index)
        {
            return ListTwo.ElementAt(index);
        }
        #endregion // Public Methods

        #region Subclasses
        public class CountChangeEventArgs : EventArgs
        {
            public int Count { get; set; }

            public CountChangeEventArgs(int count)
            {
                Count = count;
            }
        }
        #endregion // Subclasses
    }
}
