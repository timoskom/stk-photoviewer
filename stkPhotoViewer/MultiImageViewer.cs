﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace stkPhotoViewer
{
    public partial class MultiImageViewer : UserControl
    {
        #region Private Data
        private CountedDuoList<Image, string> Images { get; set; }

        private int _currentIndex = -1;
        #endregion // Private Data

        #region Public Data
        public int CurrentIndex
        {
            get => _currentIndex;
            private set
            {
                _currentIndex = value;
                if (_currentIndex == 0)
                {
                    leftListNavBtn.Enabled = false;
                    leftListNavBtn.Visible = false;
                    if (Images.Count > 1)
                    {
                        rightListNavBtn.Enabled = true;
                        rightListNavBtn.Visible = true;
                    }
                }
                else if (_currentIndex == Images.Count - 1)
                {
                    rightListNavBtn.Enabled = false;
                    rightListNavBtn.Visible = false;
                    if (Images.Count > 1)
                    {
                        leftListNavBtn.Enabled = true;
                        leftListNavBtn.Visible = true;
                    }
                }
                else
                {
                    leftListNavBtn.Enabled = rightListNavBtn.Enabled = true;
                    leftListNavBtn.Visible = rightListNavBtn.Visible = true;
                }
                EventHandler<IndexChangeEventArgs> handler = IndexChanged;
                handler(this, new IndexChangeEventArgs(_currentIndex));
            }
        }

        [
        Category("Appearance"),
        Description("The shape of the buttons.")
        ]
        public ListNavigationButton.ButtonShape ButtonsType
        {
            get => leftListNavBtn.ButtonType;
            set => leftListNavBtn.ButtonType = rightListNavBtn.ButtonType = value;
        }

        public PictureBox ImageViewer
        {
            get => listPicBx;
        }
        #endregion // Public Data

        #region Events
        public event EventHandler<IndexChangeEventArgs> IndexChanged;
        #endregion // Events

        #region Constructors
        public MultiImageViewer()
        {
            InitializeComponent();
            ButtonsType = ListNavigationButton.DefaultButtonType;
            Images = new CountedDuoList<Image, string>();
            Images.CountChanged += new EventHandler<CountedDuoList<Image, string>.CountChangeEventArgs>(images_CountChanged);
        }
        #endregion // Constructors

        #region Private Methods
        private void ChangeImage(int index)
        {
            listPicBx.Image = Images.One_GetAt(index);
            CurrentIndex = index;
        }
        #endregion // Private Methods

        #region Protected Methods
        #endregion // Protected Methods

        #region Public Methods
        public void AddImage(Image image, string file)
        {
            Images.Add(image, file);
            listPicBx.Image = Images.One_AsReadOnly().Last();
            CurrentIndex = Images.Count - 1;
        }

        public void AddRangeOfImages(IEnumerable<Image> imagesCollection, IEnumerable<string> filesCollection)
        {
            Images.AddRange(imagesCollection, filesCollection);
            listPicBx.Image = Images.One_AsReadOnly().Last();
            CurrentIndex = Images.Count - 1;
        }

        public void InsertImage(int index, Image image, string file)
        {
            Images.Insert(index, image, file);
            listPicBx.Image = Images.One_GetAt(index);
            CurrentIndex = index;
        }

        public void InsertRangeOfImages(int index, IEnumerable<Image> imagesCollection, IEnumerable<string> filesCollection)
        {
            Images.InsertRange(index, imagesCollection, filesCollection);
            listPicBx.Image = Images.One_GetAt(index + imagesCollection.Count());
            CurrentIndex = index + imagesCollection.Count();
        }

        public bool RemoveImage(Image image)
        {
            bool success = false;
            if (Images.Count > 1)
            {
                bool move = false;
                int index = Images.IndexOf(image);
                if (listPicBx.Image == image)
                    move = true;
                success = Images.Remove(image);
                if (success && move)
                {
                    if (index > 0)
                    {
                        ChangeImage(index - 1);
                    }
                    else
                    {
                        ChangeImage(0);
                    }
                }
            }
            return success;
        }

        public bool RemoveImage(string file)
        {
            bool success = false;
            if (Images.Count > 1)
            {
                bool move = false;
                int index = Images.IndexOf(file);
                if (listPicBx.Image == Images.One_GetAt(index))
                    move = true;
                success = Images.Remove(file);
                if (success && move)
                {
                    if (index > 0)
                    {
                        ChangeImage(index - 1);
                    }
                    else
                    {
                        ChangeImage(0);
                    }
                }
            }
            return success;
        }

        public void RemoveImageAt(int index)
        {
            if (index >= 0)
            {
                bool move = false;
                if (listPicBx.Image == Images.One_GetAt(index))
                    move = true;
                Images.RemoveAt(index);
                if (move)
                {
                    if (index > 0)
                    {
                        ChangeImage(index - 1);
                    }
                    else
                    {
                        ChangeImage(0);
                    }
                }
            }
        }

        public void RemoveRangeOfImages(int index, int count)
        {
            if (index >= 0 && index + count < Images.Count)
            {
                bool move = false;
                for (int i = index; i < count; i++)
                {
                    if (listPicBx.Image == Images.One_GetAt(i))
                    {
                        move = true;
                        break;
                    }
                }
                Images.RemoveRange(index, count);
                if (move)
                {
                    if (index > 0)
                    {
                        ChangeImage(index - 1);
                    }
                    else
                    {
                        ChangeImage(0);
                    }
                }
            }
        }

        public void ClearImages()
        {
            Images.Clear();
        }

        public int CountOfImages()
        {
            return Images.Count;
        }

        public Image CurrentImage()
        {
            return Images.One_GetAt(CurrentIndex);
        }

        public string CurrentFile()
        {
            return Images.Two_GetAt(CurrentIndex);
        }
        #endregion // Public Methods

        #region Event Handlers
        private void images_CountChanged(object sender, CountedDuoList<Image, string>.CountChangeEventArgs e)
        {
            if (e.Count <= 1)
            {
                leftListNavBtn.Enabled = rightListNavBtn.Enabled = false;
                leftListNavBtn.Visible = rightListNavBtn.Visible = false;
            }
            else
            {
                if (!leftListNavBtn.Visible) leftListNavBtn.Show();
                if (!leftListNavBtn.Enabled) leftListNavBtn.Enabled = true;
                if (!rightListNavBtn.Visible) rightListNavBtn.Show();
                if (!rightListNavBtn.Enabled) rightListNavBtn.Enabled = true;
            }
        }

        private void leftListNavBtn_Click(object sender, EventArgs e)
        {
            if (CurrentIndex > 0)
            {
                CurrentIndex--;
                listPicBx.Image = Images.One_GetAt(CurrentIndex);
            }
        }

        private void rightListNavBtn_Click(object sender, EventArgs e)
        {
            if (CurrentIndex < Images.Count - 1)
            {
                CurrentIndex++;
                listPicBx.Image = Images.One_GetAt(CurrentIndex);
            }
        }
        #endregion // Event Handlers

        #region Subclasses
        public class IndexChangeEventArgs
        {
            public int Index;

            public IndexChangeEventArgs(int index)
            {
                Index = index;
            }
        }
        #endregion // Subclasses
    }
}
