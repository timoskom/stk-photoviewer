﻿namespace stkPhotoViewer
{
    partial class MultiImageViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.leftArrowPnl = new System.Windows.Forms.Panel();
            this.rightArrowPnl = new System.Windows.Forms.Panel();
            this.listPicBx = new System.Windows.Forms.PictureBox();
            this.rightListNavBtn = new stkPhotoViewer.ListNavigationButton();
            this.leftListNavBtn = new stkPhotoViewer.ListNavigationButton();
            this.leftArrowPnl.SuspendLayout();
            this.rightArrowPnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listPicBx)).BeginInit();
            this.SuspendLayout();
            // 
            // leftArrowPnl
            // 
            this.leftArrowPnl.BackColor = System.Drawing.Color.Transparent;
            this.leftArrowPnl.Controls.Add(this.leftListNavBtn);
            this.leftArrowPnl.Dock = System.Windows.Forms.DockStyle.Left;
            this.leftArrowPnl.Location = new System.Drawing.Point(0, 0);
            this.leftArrowPnl.Name = "leftArrowPnl";
            this.leftArrowPnl.Size = new System.Drawing.Size(70, 400);
            this.leftArrowPnl.TabIndex = 0;
            // 
            // rightArrowPnl
            // 
            this.rightArrowPnl.BackColor = System.Drawing.Color.Transparent;
            this.rightArrowPnl.Controls.Add(this.rightListNavBtn);
            this.rightArrowPnl.Dock = System.Windows.Forms.DockStyle.Right;
            this.rightArrowPnl.Location = new System.Drawing.Point(570, 0);
            this.rightArrowPnl.Name = "rightArrowPnl";
            this.rightArrowPnl.Size = new System.Drawing.Size(70, 400);
            this.rightArrowPnl.TabIndex = 1;
            // 
            // listPicBx
            // 
            this.listPicBx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listPicBx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listPicBx.Location = new System.Drawing.Point(70, 0);
            this.listPicBx.Name = "listPicBx";
            this.listPicBx.Size = new System.Drawing.Size(500, 400);
            this.listPicBx.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.listPicBx.TabIndex = 2;
            this.listPicBx.TabStop = false;
            // 
            // rightListNavBtn
            // 
            this.rightListNavBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rightListNavBtn.BackColor = System.Drawing.Color.Transparent;
            this.rightListNavBtn.ButtonDirection = stkPhotoViewer.ListNavigationButton.ArrowButtonDirection.Right;
            this.rightListNavBtn.ButtonType = stkPhotoViewer.ListNavigationButton.ButtonShape.Square;
            this.rightListNavBtn.Location = new System.Drawing.Point(3, 168);
            this.rightListNavBtn.Name = "rightListNavBtn";
            this.rightListNavBtn.Size = new System.Drawing.Size(64, 64);
            this.rightListNavBtn.TabIndex = 0;
            this.rightListNavBtn.Visible = false;
            this.rightListNavBtn.Click += new System.EventHandler(this.rightListNavBtn_Click);
            // 
            // leftListNavBtn
            // 
            this.leftListNavBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.leftListNavBtn.BackColor = System.Drawing.Color.Transparent;
            this.leftListNavBtn.ButtonDirection = stkPhotoViewer.ListNavigationButton.ArrowButtonDirection.Left;
            this.leftListNavBtn.ButtonType = stkPhotoViewer.ListNavigationButton.ButtonShape.Square;
            this.leftListNavBtn.Location = new System.Drawing.Point(3, 168);
            this.leftListNavBtn.Name = "leftListNavBtn";
            this.leftListNavBtn.Size = new System.Drawing.Size(64, 64);
            this.leftListNavBtn.TabIndex = 0;
            this.leftListNavBtn.Visible = false;
            this.leftListNavBtn.Click += new System.EventHandler(this.leftListNavBtn_Click);
            // 
            // MultiImageViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.listPicBx);
            this.Controls.Add(this.rightArrowPnl);
            this.Controls.Add(this.leftArrowPnl);
            this.Name = "MultiImageViewer";
            this.Size = new System.Drawing.Size(640, 400);
            this.leftArrowPnl.ResumeLayout(false);
            this.rightArrowPnl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listPicBx)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel leftArrowPnl;
        private ListNavigationButton leftListNavBtn;
        private System.Windows.Forms.Panel rightArrowPnl;
        private ListNavigationButton rightListNavBtn;
        private System.Windows.Forms.PictureBox listPicBx;
    }
}
