﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace stkPhotoViewer
{
    public partial class OptionsForm : Form
    {
        public OptionsForm()
        {
            InitializeComponent();
        }

        private void backgroundColorLbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            colorDlg.Color = Properties.Settings.Default.Background_Color;
            DialogResult result = colorDlg.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                Properties.Settings.Default.Background_Color = colorDlg.Color;
            }
        }

        private void applyBtn_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
            Close();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Reload();
            Close();
        }

        private void resetBtn_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(
                this,
                "All options will be reset. Would you like to continue?",
                "WARNING",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning
            );
            if (result == DialogResult.Yes)
            {
                Properties.Settings.Default.Reset();
            }
            Close();
        }
    }
}
