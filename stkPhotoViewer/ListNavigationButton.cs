﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace stkPhotoViewer
{
    public partial class ListNavigationButton : UserControl
    {
        public enum ArrowButtonDirection
        {
            Up,
            Down,
            Left,
            Right
        }

        public enum ButtonShape
        {
            Square,
            Circle
        }

        private static readonly ArrowButtonDirection _defaultArrowButtonDirection = ArrowButtonDirection.Left;
        private static readonly ButtonShape _defaultButtonType = ButtonShape.Square;

        private ArrowButtonDirection _buttonDirection;
        private ButtonShape _buttonType;

        [
        Category("Appearance"),
        Description("The direction of the arrow.")
        ]
        public ArrowButtonDirection ButtonDirection
        {
            get => _buttonDirection;
            set
            {
                _buttonDirection = value;
                switch (value)
                {
                    case ArrowButtonDirection.Up:
                        arrowPicBx.Image = stkPhotoViewer.Properties.Resources.arrow_up_s_line;
                        break;
                    case ArrowButtonDirection.Down:
                        arrowPicBx.Image = stkPhotoViewer.Properties.Resources.arrow_down_s_line;
                        break;
                    case ArrowButtonDirection.Left:
                        arrowPicBx.Image = stkPhotoViewer.Properties.Resources.arrow_left_s_line;
                        break;
                    case ArrowButtonDirection.Right:
                        arrowPicBx.Image = stkPhotoViewer.Properties.Resources.arrow_right_s_line;
                        break;
                    default:
                        break;
                }
            }
        }

        [
        Category("Appearance"),
        Description("The shape of the button.")
        ]
        public ButtonShape ButtonType
        {
            get => _buttonType;
            set
            {
                _buttonType = value;
                InvokePaint(this, null);
            }
        }

        static public ArrowButtonDirection DefaultButtonDirection
        {
            get => _defaultArrowButtonDirection;
        }

        static public ButtonShape DefaultButtonType
        {
            get => _defaultButtonType;
        }

        public new event EventHandler Click
        {
            add
            {
                base.Click += value;
                foreach (Control control in Controls)
                {
                    control.Click += value;
                }
            }
            remove
            {
                base.Click -= value;
                foreach (Control control in Controls)
                {
                    control.Click -= value;
                }
            }
        }

        public ListNavigationButton()
        {
            InitializeComponent();
            ButtonDirection = DefaultButtonDirection;
            ButtonType = DefaultButtonType;
        }

        private void arrowPicBx_MouseEnter(object sender, EventArgs e)
        {
            arrowPicBx.BackColor = Color.FromArgb(144, 96, 96, 96);
        }

        private void arrowPicBx_MouseLeave(object sender, EventArgs e)
        {
            arrowPicBx.BackColor = Color.FromArgb(192, 192, 192);
        }

        private void arrowPicBx_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                arrowPicBx.BackColor = Color.FromArgb(255, 96, 96, 96);
        }

        private void arrowPicBx_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                arrowPicBx.BackColor = Color.FromArgb(144, 96, 96, 96);
        }

        private void arrowPicBx_Paint(object sender, PaintEventArgs e)
        {
            if (ButtonType == ButtonShape.Circle)
            {
                using (GraphicsPath gp = new GraphicsPath())
                {
                    gp.AddEllipse(0, 0, Width - 1, Height - 1);
                    Region rg = new Region(gp);
                    Region = rg;
                    e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                    e.Graphics.DrawEllipse(new Pen(new SolidBrush(BackColor), 1), 0, 0, Width - 1, Height - 1);
                }
            }
            else
            {
                base.OnPaint(e);
            }
        }
    }
}
