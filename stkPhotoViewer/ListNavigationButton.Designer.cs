﻿namespace stkPhotoViewer
{
    partial class ListNavigationButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.arrowPicBx = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.arrowPicBx)).BeginInit();
            this.SuspendLayout();
            // 
            // arrowPicBx
            // 
            this.arrowPicBx.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.arrowPicBx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.arrowPicBx.Location = new System.Drawing.Point(0, 0);
            this.arrowPicBx.Name = "arrowPicBx";
            this.arrowPicBx.Size = new System.Drawing.Size(64, 64);
            this.arrowPicBx.TabIndex = 0;
            this.arrowPicBx.TabStop = false;
            this.arrowPicBx.Paint += new System.Windows.Forms.PaintEventHandler(this.arrowPicBx_Paint);
            this.arrowPicBx.MouseDown += new System.Windows.Forms.MouseEventHandler(this.arrowPicBx_MouseDown);
            this.arrowPicBx.MouseEnter += new System.EventHandler(this.arrowPicBx_MouseEnter);
            this.arrowPicBx.MouseLeave += new System.EventHandler(this.arrowPicBx_MouseLeave);
            this.arrowPicBx.MouseUp += new System.Windows.Forms.MouseEventHandler(this.arrowPicBx_MouseUp);
            // 
            // ListNavigationButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.arrowPicBx);
            this.DoubleBuffered = true;
            this.Name = "ListNavigationButton";
            this.Size = new System.Drawing.Size(64, 64);
            ((System.ComponentModel.ISupportInitialize)(this.arrowPicBx)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox arrowPicBx;
    }
}
