﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace stkPhotoViewer
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            mainMultiViewer.IndexChanged += new EventHandler<MultiImageViewer.IndexChangeEventArgs>(mainMultiViewer_IndexChanged);
            Properties.Settings.Default.PropertyChanged += new PropertyChangedEventHandler(settingsDefault_PropertyChanged);
            ApplySavedSettings();
        }

        private void ApplySavedSettings()
        {
            mainMultiViewer.BackColor = Properties.Settings.Default.Background_Color;
        }

        #region Event Handlers
        private void mainMultiViewer_IndexChanged(object sender, MultiImageViewer.IndexChangeEventArgs e)
        {
            currentFileTSSLbl.Text = mainMultiViewer.CurrentFile();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                if (mainMultiViewer.CountOfImages() != 0)
                    mainMultiViewer.ClearImages();
                foreach (string fName in openFileDialog.FileNames)
                    mainMultiViewer.AddImage(Image.FromFile(fName, true), fName);
                currentFileTSSLbl.Text = mainMultiViewer.CurrentFile();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (mainMultiViewer.CountOfImages() == 0)
            {
                MessageBox.Show(this, "Nothing to save", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            string bufFname = mainMultiViewer.CurrentFile();
            if (bufFname.EndsWith(".bmp"))
                saveFileDialog.Filter = "Bitmap|*.bmp";
            else if (bufFname.EndsWith(".gif"))
                saveFileDialog.Filter = "GIF|*.gif";
            else if (bufFname.EndsWith(".jpg") || bufFname.EndsWith(".jpeg"))
                saveFileDialog.Filter = "JPEG|*.jpg;*.jpeg";
            else if (bufFname.EndsWith(".png"))
                saveFileDialog.Filter = "PNG|*.png";
            else if (bufFname.EndsWith(".tif") || bufFname.EndsWith(".tiff"))
                saveFileDialog.Filter = "TIFF|*.tif;*.tiff";
            DialogResult result = saveFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                string removeName = bufFname;
                bufFname = saveFileDialog.FileName;
                if (bufFname.EndsWith(".bmp"))
                    mainMultiViewer.ImageViewer.Image.Save(bufFname, System.Drawing.Imaging.ImageFormat.Bmp);
                else if (bufFname.EndsWith(".gif"))
                    mainMultiViewer.ImageViewer.Image.Save(bufFname, System.Drawing.Imaging.ImageFormat.Gif);
                else if (bufFname.EndsWith(".jpg") || bufFname.EndsWith(".jpeg"))
                    mainMultiViewer.ImageViewer.Image.Save(bufFname, System.Drawing.Imaging.ImageFormat.Jpeg);
                else if (bufFname.EndsWith(".png"))
                    mainMultiViewer.ImageViewer.Image.Save(bufFname, System.Drawing.Imaging.ImageFormat.Png);
                else if (bufFname.EndsWith(".tif") || bufFname.EndsWith(".tiff"))
                    mainMultiViewer.ImageViewer.Image.Save(bufFname, System.Drawing.Imaging.ImageFormat.Tiff);
                Image removeImage = mainMultiViewer.ImageViewer.Image;
                mainMultiViewer.RemoveImage(removeName);
                mainMultiViewer.AddImage(removeImage, bufFname);
                currentFileTSSLbl.Text = mainMultiViewer.CurrentFile();
            }
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (mainMultiViewer.CountOfImages() == 0)
            {
                MessageBox.Show(this, "Nothing to print", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            printDocument.DocumentName = mainMultiViewer.CurrentFile();
            printDialog.Document = printDocument;
            printDialog.ShowDialog(this);
        }

        private void printPreviewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (mainMultiViewer.CountOfImages() == 0)
            {
                MessageBox.Show(this, "Nothing to print", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            printDocument.DocumentName = mainMultiViewer.CurrentFile();
            printPreviewDialog.Document = printDocument;
            printPreviewDialog.ShowDialog(this);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox about = new AboutBox();
            about.ShowDialog(this);
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OptionsForm options = new OptionsForm();
            options.ShowDialog(this);
        }

        private void settingsDefault_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            mainMultiViewer.BackColor = (Color)Properties.Settings.Default[e.PropertyName];
        }

        private void printDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            Image image = mainMultiViewer.CurrentImage();
            e.Graphics.DrawImage(image, 0, 0, image.Width, image.Height);
        }
        #endregion // Event Handlers
    }
}
